from sqlalchemy import create_engine,func
from sqlalchemy.orm import sessionmaker
import classes

class Model:
    def __init__(self):
        self.session = None

    def connect(self):
        engine = create_engine('postgresql://rwzuhfek:Op5iyR8BcH2-qm5gBazC2Mtv6Dpft6z0@ziggy.db.elephantsql.com/rwzuhfek')
        try:
            engine.connect()
            self.session = sessionmaker(bind=engine)()
            print('conection exist')
        except:
            print("Error connection with PostgreSQL replica, use of master power")
        engine.dispose()

    def close(self):
        try:
            if(self.session!=None):self.session.close()
            print("Connection closed")
        except Exception as error:print("Error close connection with PostrgreSQL",error)

    #Master funtion
    def add_new_item(self, new_item):
        try:
            self.session.add(new_item)
            self.session.commit()
            return new_item
        except Exception as exp:
            print('You have problem with adding item. Detail info: %s' % exp)

    def update_item(self):
        try:
            self.session.commit()
        except Exception as exp:
            print('You have problem with update item. Detail info: %s' % exp)
    
    def delete_word(self,word):
        try:
            self.session.delete(word)
            self.session.commit()
        except Exception as exp:
            print('You have problem with delete item. Detail info: %s' % exp)
    
    def get_photo(self, names):
        return self.session.query(classes.Photos).filter(classes.Photos.name==names).all()

    def find_word(self,eng_word,ukr_word):
        return self.session.query(classes.Words).filter(classes.Words.english==eng_word,classes.Words.ukraine==ukr_word).first()

    def find_words(self,word):
        return self.session.query(classes.Words).filter(classes.Words.english.contains(word)).order_by(classes.Words.english,classes.Words.ukraine).all()