import asyncio
import logging
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types.message import ContentType
from aiogram.utils import executor
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.markdown import text, bold, italic, code, pre
from aiogram.types import ParseMode

from io import BytesIO,BufferedReader

from button import choise
import classes
from database import Model
model = Model()
model.connect()

storage = MemoryStorage()
bot = Bot(token="1568084047:AAHIy5SPvER50xAg6Z2q5M-PT_3tZsDRPzg")
dp = Dispatcher(bot, storage=storage)

@dp.message_handler(commands=["start","help"])
async def cmd_start(message: types.Message):
    await message.answer(text="Для пошуку просто введіть слово\n"
                              "/start|/help - переглянути можливі команди\n"
                              "/add - додати новий запис\n"
                              "/upload - додати url адресу до певного слова\n"
                              "/update - оновити існуючий запис\n"
                              "/delete - видалити певний запис\n"
                              "/prepositions - переглянути певні прийменники\n"
                              "/times - переглянути табличку часів")

@dp.message_handler(commands=["prepositions"])
async def prepositions_info(message: types.Message):
    find_text = model.get_photo(message.text.lower().split('/')[1])
    for photo in find_text:
        await bot.send_photo(message.from_user.id, photo.photo)

class Form_upload(StatesGroup):
    download_name = State()
    download_url = State()

@dp.message_handler(commands=['upload'])
async def upload_item(message: types.Message):
    await Form_upload.download_name.set()
    await message.reply("Enter name photo")

@dp.message_handler(state=Form_upload.download_name)
async def upload_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text.lower()
    await Form_upload.next()
    await message.answer("Enter photo url")

@dp.message_handler(state=Form_upload.download_url)
async def upload_url(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['url'] = message.text.lower()
        model.add_new_item(classes.Photos(data['name'], data['url'] ))

    await state.finish()

@dp.message_handler(commands=["times"])
async def times_info(message: types.Message):
    find_text = model.get_photo(message.text.lower().split('/')[1])
    for photo in find_text:
        await bot.send_photo(message.from_user.id, photo.photo)

'''Add new word'''
class Form_add(StatesGroup):
    english = State()
    ukraine = State()

@dp.message_handler(commands=['add'])
async def add_item_process(message: types.Message):
    await Form_add.english.set()
    await message.reply("Enter english word")

@dp.message_handler(state=Form_add.english)
async def process_add_english(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['english'] = message.text.lower()
    await Form_add.next()
    await message.answer("Enter ukraine translate")

@dp.message_handler(state=Form_add.ukraine)
async def process_add_ukraine(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ukraine'] = message.text.lower()
        model.add_new_item(classes.Words(data['english'],data['ukraine'],message.from_user.username))
        await message.answer(text="English: {}\nUkraine: {}".format(data['english'],data['ukraine']))

    await state.finish()

'''Update word'''
class Form_update(StatesGroup):
    english_old = State()
    english_new = State()
    ukraine_old = State()
    ukraine_new = State()

@dp.message_handler(commands=['update'])
async def update_item(message: types.Message):
    await Form_update.english_old.set()
    await message.reply("Enter english old word")

@dp.message_handler(state=Form_update.english_old)
async def process_find_english(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['english old'] = message.text.lower()
    await Form_update.next()
    await message.answer("Enter english new word")

@dp.message_handler(state=Form_update.english_new)
async def process_update_english(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['english new'] = message.text.lower()
    await Form_update.next()
    await message.answer("Enter ukraine old translate")

@dp.message_handler(state=Form_update.ukraine_old)
async def process_find_ukraine(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ukraine old'] = message.text.lower()
    await Form_update.next()
    await message.answer("Enter ukraine new translate")

@dp.message_handler(state=Form_update.ukraine_new)
async def process_update_ukraine(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ukraine new'] = message.text.lower()
        word=model.find_word(data['english old'],data['ukraine old'])
        if(word!=None):
            word.english=data['english new']
            word.ukraine=data['ukraine new']
            word.user=message.from_user.username
            model.update_item()
            await message.answer(text="Word {}:{} -> {}:{} seccesffuly updated".format(data['english old'],data['ukraine old'],data['english new'],data['ukraine new']))
        else:
            await message.answer(text="Word not found, try againe")
    await state.finish()

'''Delete word'''
class Form_delete(StatesGroup):
    english = State()
    ukraine = State()

@dp.message_handler(commands=['delete'])
async def add_item(message: types.Message):
    await Form_delete.english.set()
    await message.reply("Enter english word")

@dp.message_handler(state=Form_delete.english)
async def process_english(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['english'] = message.text.lower()
    await Form_delete.next()
    await message.answer("Enter ukraine translate")

@dp.message_handler(state=Form_delete.ukraine)
async def process_ukraine(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ukraine'] = message.text.lower()
        word=model.find_word(data['english'],data['ukraine'])
        if(word!=None):
            model.delete_word(word)
            await message.answer(text="Word {} -> {} seccesffuly deleted".format(data['english'],data['ukraine']))
        else:
            await message.answer(text="Word not found, try againe")
    await state.finish()

'''Find word'''
@dp.message_handler(content_types=ContentType.TEXT)
async def find_word_translate_command(msg: types.Message):
    find_text = model.find_words(msg.text.lower())
    find_list_word = "Translate not found"
    if(find_text!=[]):
        find_list_word=""
        i=0
        while True:
            try:
                find_list_word += "{} -> {}\n".format(find_text[i].english.strip(), find_text[i].ukraine)
                i+=1
            except: 
                if msg.text.lower().strip()==find_text[0].english.lower().strip():
                    text = model.get_photo(msg.text.lower().strip())
                    for photo in text:
                        try:
                            await bot.send_photo(msg.from_user.id, photo.photo)
                        except:await msg.answer(text="url adress not found")
                        
                break
    await msg.answer(text=find_list_word)
    

if __name__ == '__main__':
    executor.start_polling(dp)