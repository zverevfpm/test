from aiogram.types import InlineKeyboardMarkup,InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

select_callback = CallbackData("select","item_lenguige")

choise = InlineKeyboardMarkup(row_width=3)

eng_button=InlineKeyboardButton(text="English", callback_data="select:english")
choise.insert(eng_button)

ukr_button=InlineKeyboardButton(text="Ukraine", callback_data="select:ukraine")
choise.insert(ukr_button)

cancel_button=InlineKeyboardButton(text="Cancel", callback_data="cancel")
choise.insert(cancel_button)