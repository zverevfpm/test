from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class Words(Base):
    __tablename__ = 'data'
    word_id = Column('word_id',Integer, autoincrement=True, primary_key=True)
    english = Column('english', String(100))
    ukraine = Column('ukraine', String(100))
    user = Column('user',String(50))

    def __init__(self, english,ukraine,user):
        self.english = english
        self.ukraine = ukraine
        self.user = user

class Photos(Base):
    __tablename__ = 'photos'
    name = Column('name',String, primary_key=True)
    photo = Column('url', String,primary_key=True)

    def __init__(self, name,photo):
        self.name = name
        self.photo = photo